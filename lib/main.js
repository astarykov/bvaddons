// Import the page-mod API
var pageMod = require("sdk/page-mod");
var self = require("sdk/self");
var tabs = require('sdk/tabs');

// Create a page mod
// It will run a script whenever a "jira" URL is loaded
pageMod.PageMod({
  include: "https://bits.bazaarvoice.com/jira/browse/*",
        contentScriptFile: self.data.url("jira.js"),
        contentStyleFile: self.data.url("style.css")
});